# Author: Heng Liu
# Contact: hengl@email.arizona.edu


import tensorflow as tf
import numpy as np
import os
import sys
import collections



class PTB(object):

	def __init__(self, training):
		self.batch_size = 16
		self.num_steps = 32
		self.vocab = 10000
		self.hidden = 200
		self.layers = 2
		self.path = ''
		self.keep_prob = 0.5
		self.learning_rate = 1
		self.max_grad_norm = 5

		self.input_data = tf.placeholder(tf.int32, [self.batch_size, self.num_steps])
		self.input_label = tf.placeholder(tf.int32, [self.batch_size, self.num_steps])

		cell_lstm = tf.nn.rnn_cell.BasicLSTMCell(self.hidden)
		cell_lstm = tf.nn.rnn_cell.DropoutWrapper(cell_lstm, output_keep_prob = self.keep_prob) #this adds dropout to input/output/state
		cells = tf.nn.rnn_cell.MultiRNNCell([cell_lstm] * self.layers)

		# this is the initializer of the state for all RNN cells, this obtains lstm states augmented by batch_size, and initialize them
		self.initial_states = cells.zero_state(self.batch_size, tf.float32)  

		embedding = tf.get_variable('embedding', [self.vocab, self.hidden]) #still don't understand how the data is organized/extracted
		# Note this is word2vec repres. instead of 1-in-k encoding. What we did here is transformed a unique word id to a vector representation
		inputs = tf.nn.embedding_lookup(embedding, self.input_data)  
		inputs = tf.nn.dropout(inputs, self.keep_prob)  #perform the dropout on input data

		outputs = []  #this is used to collect the predicted labels

		state = self.initial_states # 'state' is updated iteratively
		
		with tf.variable_scope('rnn'):
			for i in range(self.num_steps):
				if i > 0:  #if this is not the first time accessing the variable
					tf.get_variable_scope().reuse_variables()
				cell_out, state = cells(inputs[:, i ,:], state)  #cell_out tensor is of shape batch * hidden, num_steps of them in total
				outputs.append(cell_out) 
		
		outputs = tf.concat(outputs, 1) # concatnate along dime1 (hidden), the result is of size batch by (num_steps * hidden)
		outputs = tf.reshape(outputs, [-1, self.hidden]) #reshape the result to make the 2nd dim to be hidden, the first dim is automatically calculated (num_step * batch)
		
		out_weights = tf.get_variable('weight', [self.hidden, self.vocab])
		out_bias = tf.get_variable('bias', [self.vocab])
		logits = tf.matmul(outputs, out_weights) + out_bias  # of shape (num_step * batch) by vocab
		
		loss = tf.contrib.legacy_seq2seq.sequence_loss_by_example([logits],  #y^hat
					[tf.reshape(self.input_label, [-1])],  # target labels, [batch_size, num_steps] compressed to 1d tensor
					[tf.ones([self.batch_size * self.num_steps], dtype=tf.float32)])

		self.cost = tf.reduce_sum(loss)/self.batch_size
		self.final_state = state
		if training == False:
			return

		trainable_variable = tf.trainable_variables()
		gradients = tf.gradients(self.cost, trainable_variable)  #this gets the derivatives of cost w.r.t. trainable_variable()
		grads, _ = tf.clip_by_global_norm(gradients, self.max_grad_norm)
		optimizer = tf.train.GradientDescentOptimizer(self.learning_rate)
		self.train_op = optimizer.apply_gradients(zip(grads, trainable_variable))
		

def _read_words(filename):
	with tf.gfile.GFile(filename, "r") as f:
		if sys.version[0] == 3: #if python3
			return f.read().replace("\n", "<eos>").split()
		else:
			return f.read().decode("utf-8").replace("\n", "<eos>").split()
 

def _build_vocab(filename):
	data = _read_words(filename)
 
	counter = collections.Counter(data)
	count_pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))
 
	words, _ = list(zip(*count_pairs))
	word_to_id = dict(zip(words, range(len(words))))
 
	return word_to_id
 
 
def _file_to_word_ids(filename, word_to_id):
	data = _read_words(filename)
	return [word_to_id[word] for word in data if word in word_to_id]
 
 
def ptb_raw_data(data_path=None):
	train_path = os.path.join(data_path, "ptb.train.txt")
	valid_path = os.path.join(data_path, "ptb.valid.txt")
	test_path = os.path.join(data_path, "ptb.test.txt")
 
	word_to_id = _build_vocab(train_path)
	train_data = _file_to_word_ids(train_path, word_to_id)
	valid_data = _file_to_word_ids(valid_path, word_to_id)
	test_data = _file_to_word_ids(test_path, word_to_id)
	vocabulary = len(word_to_id)
	return train_data, valid_data, test_data, vocabulary
	
		
def main():

	# define two separate models 
	with tf.variable_scope('model', reuse = None, initializer = tf.random_uniform_initializer(-0.05, 0.05)):
		train_model = PTB(True)
	# THe evaluation model will reuse previous model's variables
	with tf.variable_scope('model', reuse = True, initializer = tf.random_uniform_initializer(-0.05, 0.05)):
		eval_model = PTB(False)

	train_raw, valid_raw, test_raw, dictionary = ptb_raw_data(train_model.path)
	sess = tf.Session()

	#return tf.data.Datasets for input and labels
	base_data = tf.convert_to_tensor(train_raw)
	batch_len = tf.size(train_raw) // train_model.batch_size  #count how many batches, then we can consider the time_num 
	base_data = tf.reshape(base_data[0: train_model.batch_size * batch_len], [train_model.batch_size, batch_len])  #tf.reshape(tensor, shape_desired)
	#because we use time_t+1 as label for time_t word, so we deduct one word
	epoch_size = (batch_len - 1) // train_model.num_steps   # 'num_steps' columns as a group

	base_data_eval = tf.convert_to_tensor(valid_raw)
	batch_len_eval = tf.size(valid_raw) // eval_model.batch_size
	base_data_eval = tf.reshape(base_data_eval[0: eval_model.batch_size * batch_len_eval], [eval_model.batch_size, batch_len_eval])
	epoch_size_eval = (batch_len_eval - 1) // eval_model.num_steps

	
	with sess:
		indices = tf.data.Dataset.from_tensor_slices(tf.constant(range(epoch_size.eval())))  #here we create the indices referring to each epoch
		#In order to refer/get inside a tensor, we use following function, here first para is the array operated on, 
		#second para is the starting position of two dims, second para is the stopping position of two dims.
		train_data = indices.map(lambda l: tf.strided_slice(base_data, [0, l*train_model.num_steps], [train_model.batch_size, (l+1)*train_model.num_steps]))
		train_label = indices.map(lambda l: tf.strided_slice(base_data, [0, l*train_model.num_steps + 1], [train_model.batch_size, (l+1)*train_model.num_steps + 1]))

		indices = tf.data.Dataset.from_tensor_slices(tf.constant(range(epoch_size_eval.eval())))
		eval_data = indices.map(lambda l: tf.strided_slice(base_data_eval, [0, l*eval_model.num_steps], [eval_model.batch_size, (l+1)*eval_model.num_steps]))
		eval_label = indices.map(lambda l: tf.strided_slice(base_data_eval, [0, l*eval_model.num_steps + 1], [eval_model.batch_size, (l+1)*eval_model.num_steps + 1]))
		
		iterator_data = train_data.make_one_shot_iterator()
		iterator_label = train_label.make_one_shot_iterator()

		iterator_data_eval = eval_data.make_one_shot_iterator()
		iterator_label_eval = eval_label.make_one_shot_iterator()

		sess.run(tf.global_variables_initializer())
		
		states = sess.run(train_model.initial_states) # the intial states need to be executed first to feed to next step

		next_data = iterator_data.get_next()  # these need to be outside the loop
		next_label = iterator_label.get_next()
		next_data_eval = iterator_data_eval.get_next()  # these need to be outside the loop
		next_label_eval = iterator_label_eval.get_next()
		
		total_cost = 0.0
		itera = 0
		for i in range(epoch_size.eval()):
			# when feeding the values, tensor can't be used
			cost, states, _  = sess.run([train_model.cost, train_model.final_state, train_model.train_op], 
						{train_model.input_data: next_data.eval(), train_model.input_label: next_label.eval(), train_model.initial_states: states})
			total_cost += cost
			itera += train_model.num_steps
			if i%100 == 0:
				print("After %d steps, perplexity is %.3f" % (i, np.exp(total_cost / itera)))

		total_cost = 0.0
		itera = 0
		for i in range(epoch_size_eval.eval()):
			cost, states, _ = sess.run([eval_model.cost, eval_model.final_state, tf.no_op()], 
						{eval_model.input_data: next_data_eval.eval(), eval_model.input_label: next_label_eval.eval(), eval_model.initial_states: states})
			total_cost += cost
			itera += eval_model.num_steps
			if i%30 == 0:
				print("After %d steps, perplexity is %.3f" % (i, np.exp(total_cost / itera)))
			



if __name__ == "__main__":
	main() 












